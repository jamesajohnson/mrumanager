﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegisteryTest
{
    public partial class Form1 : Form
    {
        private MRUManager mruManager;

        public Form1()
        {
            InitializeComponent();

            //initialise mruManager
            mruManager = new MRUManager("Chef IDE");

            //create mru menu drop down & onclick event
            ToolStripMenuItem mruMenu = mruManager.BuildMenu();
            mruMenu.DropDownItemClicked += MruMeun_DropDownItemClicked;

            //add Mru menu to menu bar
            fileToolStripMenuItem.DropDownItems.Add(mruMenu);            
        }

        //mru file selected
        private void MruMeun_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem tsi = (ToolStripItem)e.ClickedItem;
            Console.WriteLine("MRU File Opened: " + tsi.Text);
        }

        //add a file to the MRU list when opening
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mruManager.AddFile(openFileDialog1.FileName);
            }
        }
    }
}
