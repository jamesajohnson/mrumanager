﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegisteryTest
{
    class MRUManager
    {
        private int max = 10;
        private string programName = string.Empty;

        private RegistryKey softwareKey;
        private RegistryKey thisKey;
        private RegistryKey mruKey;

        public MRUManager(string name)
        {
            programName = name;
        }

        public ToolStripMenuItem BuildMenu()
        {
            ToolStripMenuItem menu = new ToolStripMenuItem();
            menu.Text = "Recently Opened Files";

            foreach (string file in ListFiles())
            {
                //build sub menu
                ToolStripMenuItem tsmFile = new ToolStripMenuItem();
                tsmFile.Text = file;
                menu.DropDownItems.Add(tsmFile);
            }

            return menu;
        }

        public List<string> ListFiles()
        {
            openKeys();

            //list of files to return
            List<string> returnFiles = new List<string>();
            //list of dates in registery
            string[] dates = mruKey.GetSubKeyNames();

            //open each date foled in mru key and read to file name to returnFiles
            Microsoft.Win32.RegistryKey dateKey;
            foreach (string date in dates)
            {
                dateKey = mruKey.OpenSubKey(date, true);
                //user insert 0 to push item onto list
                returnFiles.Insert(0, dateKey.GetValue("File").ToString());
            }

            closeKeys();

            return returnFiles;
        }

        public void AddFile(string fileName)
        {
            //delete file from list if it exists
            if (fileAlreadListed(fileName))
            {
                deleteFile(fileName);
            }

            openKeys();

            //create key in mru with current date, add file value to that key
            Microsoft.Win32.RegistryKey fileKey = mruKey.CreateSubKey(DateTime.Now.ToString());
            fileKey.SetValue("File", fileName);
            fileKey.Close();

            clipMRUList();

            closeKeys();
        }

        // Has the file list excided max? remove first file
        private void clipMRUList()
        {
            string[] mruFiles = mruKey.GetSubKeyNames();

            if (mruFiles.Length > max)
            {
                mruKey.DeleteSubKeyTree(mruFiles[0]);
            }
        }

        //is the file already listed
        private bool fileAlreadListed(string fileName)
        {
            if (ListFiles().Contains(fileName))
                return true;
            else
                return false;
        }

        //is the file already listed
        private void deleteFile(string fileName)
        {
            openKeys();

            //list of dates in registery
            string[] dates = mruKey.GetSubKeyNames();

            //open each date foled in mru key and read to file name to returnFiles
            Microsoft.Win32.RegistryKey dateKey;
            foreach (string date in dates)
            {
                dateKey = mruKey.OpenSubKey(date, true);
                //user insert 0 to push item onto list
                if (fileName == dateKey.GetValue("File").ToString())
                {
                    mruKey.DeleteSubKeyTree(date);
                }
            }

            closeKeys();
        }

        private void openKeys()
        {
            softwareKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE", true);

            try
            {
                //the program is already listed in the registery
                thisKey = softwareKey.OpenSubKey(programName, true);
                mruKey = thisKey.OpenSubKey("MRU", true);
            }
            catch (NullReferenceException nullRef)
            {
                //the program is not listed in the registery
                thisKey = softwareKey.CreateSubKey(programName);
                mruKey = thisKey.CreateSubKey("MRU");
            }
        }

        private void closeKeys()
        {
            mruKey.Close();
            thisKey.Close();
            softwareKey.Close();
        }
    }
}
